### Welcome to the Skinnr Development team ###

* Welcome, Skinnr is an Avatar development kit.

### New to Git? Lets set up everything ###

* Firstly to get started you will need to have access to git: [Download](http://git-scm.com/downloads)
* We will need to get you started with the initial build, so ensure that you have installed Git and have it running. Let's start.


### Pulling the code to your local machine ###

1. Go into the Android Studio projects folder, create a folder and open it.
2. In the folder, right click and open git bash. From there let it load and enter this command: git init
3. Now enter this command: git remote add origin https://yourbitbucketusername@bitbuclet.org/thearc/skinnr.git
4. Now run the command: git pull origin master
5. Wait a few seconds and you will see another line asking you for your password, enter your password incorrectly (nothing will show up) and hit enter.
6. Wait while git fetches the files and adds it to your local machine.

### Pushing to your branch ###

* To ensure safety in our master branch, every contributor must make a specific branch for their work, so their code doesn't interfere with the master branch. Once you have added the new working features and code to your branch and you can confirm that it is working, you can merge the code to master. Before merging please confirm with a developer and let them review your code.

1. Open git bash in your project folder (On your local machine).
2. Click "Create Branch" on the left panel of this site and create a branch. Name it your name (Something simple).
3. Before git can push your code, it needs to know that your code has been changed. You can override the method with add .

* Pushing code to you branch.
1. git commit -m "Commit note" -a
2. git push -u origin yourbranchname
3. wait some time and it should ask for your pass, enter it and it'll do it's thing.
4. If it fails to push, run git add . (With the dot) and start from step 1 again.

* Now everything is set up correctly, you can make changes to your code and push it to your branch. When saving somehting in your project in AS you may get a popup saying save to git. Hit yes, to know the code check is working, any file you edit will be highlighted with a blue colour.

### Need more help? ###

* Conatact repo owner (Krish) or author of project (Florian).
* If you're familiar with git and this process, please edit this readme and your name to it.
* If you find any errors in this readme, edit and fix it.